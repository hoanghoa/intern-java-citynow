package com.example.simplecalculator;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {

    @Test
    public void TestCal(){
        /*
         * Test phep cong
         */
        Calculator plusCal = new Plus();
        float plusRes = 0;
        plusRes = plusCal.getResult(2,4);

        /*
         * Test phep tru
         */
        Calculator subCal = new Sub();
        float subRes = 0;
        subRes = subCal.getResult(2,4);

        /*
         * Test phep nhan
         */
        Calculator multiCal = new Multi();
        float multiRes = 0;
        multiRes = multiCal.getResult(2,4);

        /*
         * Test phep chia
         */
        Calculator divCal = new Div();
        float divRes = 0;
        divRes = divCal.getResult(2,4);

        /*
         * array ket qua va array mong muon
         */
        float []expect = new float[]{6, -2, 8, (float) 0.5};
        float []result = new float[]{plusRes, subRes, multiRes, divRes};

        assertArrayEquals(expect, result, (float) 0.1);
    }

}