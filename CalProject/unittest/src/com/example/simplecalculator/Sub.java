package com.example.simplecalculator;

public class Sub implements Calculator{
    @Override
    public float getResult(float a, float b) {
        return a - b;
    }
}
