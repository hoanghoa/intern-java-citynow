package com.example.simplecalculator;

public interface Calculator {
    public float getResult(float a, float b);
}
