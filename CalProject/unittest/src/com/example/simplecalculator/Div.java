package com.example.simplecalculator;

public class Div implements Calculator{
    @Override
    public float getResult(float a, float b) {
        if(b != 0)
            return a/b;
        else {
            return 0;
        }
    }
}
