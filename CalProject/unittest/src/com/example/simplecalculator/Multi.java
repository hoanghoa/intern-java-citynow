package com.example.simplecalculator;

public class Multi implements Calculator{
    @Override
    public float getResult(float a, float b) {
        return a * b;
    }
}
