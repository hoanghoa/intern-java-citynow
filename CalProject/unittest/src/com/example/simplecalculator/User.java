package com.example.simplecalculator;

import java.util.Scanner;

public class User {
    public static void main(String[] args) {

        /*
        * Khai bao bien
         */
        float  a;
        float  b;
        float  result;
        int    option;
        String resultMess;

        /*
        * Nhap so a
         */
        Scanner scA = new Scanner(System.in);
        System.out.print("So a: ");
        a = scA.nextFloat();

        /*
        * Nhap so b
         */
        Scanner scB = new Scanner(System.in);
        System.out.print("So b: ");
        b = scA.nextFloat();

        /*
        * Khai bao phep toan
         */
        Calculator plusCal  = new Plus();
        Calculator subCal   = new Sub();
        Calculator multiCal = new Multi();
        Calculator divCal   = new Div();

        /*
        * Chon phep tinh
         */

        System.out.println("Chon phep tinh:\n"+
                            "1. Cong \n"+
                            "2. Tru\n"+
                            "3. Nhan\n"+
                            "4. Chia\n");
        Scanner op = new Scanner(System.in);
        option = op.nextInt();

        /*
        * Chia option da chon
         */
        switch (option){
            case 1:
                result = plusCal.getResult(a, b);
                resultMess = "a + b = " + result;
                break;
            case 2:
                result = subCal.getResult(a, b);
                resultMess = "a - b = " + result;
                break;
            case 3:
                result = multiCal.getResult(a, b);
                resultMess = "a * b = " + result;
                break;
            case 4:
                result = divCal.getResult(a, b);
                if(result != 0)
                    resultMess = "a / b = ";
                else
                    resultMess = "Khong the chia cho " + (int)result;
                break;
            default:
                result = 0;
                resultMess = "";
                break;
        }

        System.out.println(resultMess);
    }
}
