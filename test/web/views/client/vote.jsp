<%--
  Created by IntelliJ IDEA.
  User: hoang
  Date: 7/24/2020
  Time: 5:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String usName = (String) session.getAttribute("usName");
    int usRole = (int) session.getAttribute("usRole");

    System.out.println("homepage: "+ usName + " " + usRole);
%>
<!DOCTYPE html>
<html lang="vn">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <title>Create Vote page</title>
</head>
<script>
    function noBack()
    {
        window.history.forward()
    }
    noBack();
    window.onload = noBack;
    window.onpageshow = function(evt) { if (evt.persisted) noBack() }
    window.onunload = function() { void (0) }
</script>
<body>
<!-- Navbar -->
<nav class="container navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Welcome <b>Username</b></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav mr-auto">
    </ul>
    <div class="form-inline my-2 my-lg-0">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <button type="button" onclick="javascript:history.back();" class="btn btn-outline-warning">Back</button>
            </li>
            <li class="nav-item active" style="margin-left: 5px;">
                <form action="<%=request.getContextPath()%>/login" method="get">
                    <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Logout</button>
                </form>
            </li>
        </ul>
    </div>
</nav>
<!-- Vote create region -->
<div class="container border border-dark">
    <div class="col-sm-12 mt-4 mb-4 border border-dark">
        <form action="<%=request.getContextPath()%>/initVote" method="post">
            <div class="row p-2">
                <div class="column col-4" style="margin-left: 85px;">
                    <label style="margin-top: 10px;">Title</label>
                    <input name="vote_title" type="text" class="form-control" required>
                    <label style="margin-top: 10px;">Question</label>
                    <textarea name="vote_question" class="form-control" required></textarea>
                </div>
                <div class="col-2">
                </div>
                <div class="column col-4">
                    <label style="margin-top: 10px;">Option 1</label>
                    <input name="vote_op1" type="text" class="form-control" required>
                    <label style="margin-top: 10px;">Option 2</label>
                    <input name="vote_op2" type="text" class="form-control" required>
                    <label style="margin-top: 10px;">Option 3</label>
                    <input name="vote_op3" type="text" class="form-control" required>
                    <label style="margin-top: 10px;">Option 4</label>
                    <input name="vote_op4" type="text" class="form-control" required>
                </div>
            </div>
            <div class="col-12 d-flex justify-content-center" style="margin-bottom: 20px; margin-top: 30px;">
                <div class="row d-flex justify-content-center">
                    <button type="button" class="btn btn-outline-dark" onclick="window.location.href=window.location.href">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-outline-primary" style="margin-left: 40px;">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>

</html>
