<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>

<%
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <script src="https://kit.fontawesome.com/8b1db1262c.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../css/loginstyle.css">
    <link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <title></title>
</head>
<script>
    // function readURL(input) {
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //
    //         reader.onload = function (e) {
    //             $('#blah')
    //                 .attr('src', e.target.result)
    //                 .width(150)
    //                 .height(150);
    //         };
    //
    //         reader.readAsDataURL(input.files[0]);
    //     }
    // }
</script>
<body>
<div class="container" >
    <h1 style="color:#000">SIGN UP</h1>
    <form action="<%=request.getContextPath()%>/signUp" method="post" enctype="multipart/form-data">

        <div class="tbox">
            <input type="text" placeholder="fullName" name="fullName" required>
        </div>

        <div class="tbox">
            <input type="text" placeholder="username" name="accountName" required>
        </div>

        <div class="tbox">
            <input type="text" placeholder="email" name="email" required>
        </div>

        <div class="tbox">
            <input type="password" placeholder="password" name="resPassword" required>
        </div>

        <div class="tbox">
            <input type="file" name = "resAvt" required />
<%--            <img id="blah" name="resAvt" src="#" alt="your image" />--%>
        </div>

        <input class="btn" type="submit" name="" value="Register">
    </form>

    <%--    <a class="b1" href="#">FORGOT PASSWORD ? </a>--%>
    <%--    <a class="b2" href="#">CREATE AN ACCOUNT </a>--%>

</div>
</body>
</html>
