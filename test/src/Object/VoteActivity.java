package Object;

import java.io.Serializable;
import java.sql.Timestamp;

public class VoteActivity implements Serializable {
    private int va_id;
    private int va_uid;
    private int va_vid;
    private String va_content;
    private Timestamp va_createAt;

    public int getVa_id() {
        return va_id;
    }

    public void setVa_id(int va_id) {
        this.va_id = va_id;
    }

    public int getVa_uid() {
        return va_uid;
    }

    public void setVa_uid(int va_uid) {
        this.va_uid = va_uid;
    }

    public int getVa_vid() {
        return va_vid;
    }

    public void setVa_vid(int va_vid) {
        this.va_vid = va_vid;
    }

    public String getVa_content() {
        return va_content;
    }

    public void setVa_content(String va_content) {
        this.va_content = va_content;
    }

    public Timestamp getVa_createAt() {
        return va_createAt;
    }

    public void setVa_createAt(Timestamp va_createAt) {
        this.va_createAt = va_createAt;
    }
}
