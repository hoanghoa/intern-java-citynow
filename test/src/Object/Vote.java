package Object;

import java.io.Serializable;
import java.sql.Timestamp;

public class Vote implements Serializable {
    private int vid;
    private int uid;
    private String title;
    private String question;
    private Timestamp create_at;
    private Timestamp update_at;

    public int getVid() {
        return vid;
    }

    public void setVid(int vid) {
        this.vid = vid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Timestamp getCreate_at() {
        return create_at;
    }

    public void setCreate_at(Timestamp create_at) {
        this.create_at = create_at;
    }

    public Timestamp getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(Timestamp update_at) {
        this.update_at = update_at;
    }

    public String voteInfo(){
        return this.getUid() + this.getTitle() + this.getQuestion() + this.getCreate_at() + this.getUpdate_at();
    }
}
