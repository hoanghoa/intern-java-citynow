package Object;

import java.io.Serializable;
import java.sql.Timestamp;

public class Comment implements Serializable {
    private int c_id;
    private int c_uid;
    private int c_vid;
    private String c_content;
    private Timestamp c_createAt;

    public int getC_id() {
        return c_id;
    }

    public void setC_id(int c_id) {
        this.c_id = c_id;
    }

    public int getC_uid() {
        return c_uid;
    }

    public void setC_uid(int c_uid) {
        this.c_uid = c_uid;
    }

    public int getC_vid() {
        return c_vid;
    }

    public void setC_vid(int c_vid) {
        this.c_vid = c_vid;
    }

    public String getC_content() {
        return c_content;
    }

    public void setC_content(String c_content) {
        this.c_content = c_content;
    }

    public Timestamp getC_createAt() {
        return c_createAt;
    }

    public void setC_createAt(Timestamp c_createAt) {
        this.c_createAt = c_createAt;
    }

    public String info(){
        return this.c_id + " " + this.c_uid + " " + this.c_vid + " " + this.c_content + " " + this.c_createAt;
    }
}
