package Object;

import java.io.Serializable;
import java.sql.Blob;

public class TopVote implements Serializable {
    private Blob avatar;
    private String fullName;
    private int votedNum;
    private int commentedNum;

    public Blob getAvatar() {
        return avatar;
    }

    public void setAvatar(Blob avatar) {
        this.avatar = avatar;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getVotedNum() {
        return votedNum;
    }

    public void setVotedNum(int votedNum) {
        this.votedNum = votedNum;
    }

    public int getCommentedNum() {
        return commentedNum;
    }

    public void setCommentedNum(int commentedNum) {
        this.commentedNum = commentedNum;
    }
}
