package Object;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Timestamp;

public class CommentContent implements Serializable {
    private int c_id;
    private Blob c_uAvatar;
    private String c_fullName;
    private Timestamp c_time;
    private String c_content;

    public Blob getC_uAvatar() {
        return c_uAvatar;
    }

    public void setC_uAvatar(Blob c_uAvatar) {
        this.c_uAvatar = c_uAvatar;
    }

    public int getC_id() {
        return c_id;
    }

    public void setC_id(int c_id) {
        this.c_id = c_id;
    }

    public String getC_fullName() {
        return c_fullName;
    }

    public void setC_fullName(String c_fullName) {
        this.c_fullName = c_fullName;
    }

    public Timestamp getC_time() {
        return c_time;
    }

    public void setC_time(Timestamp c_time) {
        this.c_time = c_time;
    }

    public String getC_content() {
        return c_content;
    }

    public void setC_content(String c_content) {
        this.c_content = c_content;
    }
}
