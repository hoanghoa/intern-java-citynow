package Object;

import java.io.Serializable;

public class TestClass implements Serializable {
    private int voteNum;
    private int vid;

    public int getVoteNum() {
        return voteNum;
    }

    public void setVoteNum(int voteNum) {
        this.voteNum = voteNum;
    }

    public int getVid() {
        return vid;
    }

    public void setVid(int vid) {
        this.vid = vid;
    }
}
