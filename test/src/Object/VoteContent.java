package Object;

import java.io.Serializable;

public class VoteContent implements Serializable {
    private int vc_vid;
    private String vc_content;

    public int getVc_vid() {
        return vc_vid;
    }

    public void setVc_vid(int vc_vid) {
        this.vc_vid = vc_vid;
    }

    public String getVc_content() {
        return vc_content;
    }

    public void setVc_content(String vc_content) {
        this.vc_content = vc_content;
    }
}
