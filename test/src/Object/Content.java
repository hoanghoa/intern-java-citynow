package Object;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.List;

public class Content implements Serializable {
    private int v_id;
    private Blob v_uAvatar;
    private String v_uName;
    private String v_accName;
    private String v_title;
    private String v_question;
    private List<String> v_option;
    private Timestamp v_createAt;
    private Timestamp v_updateAt;


    public Blob getV_uAvatar() {
        return v_uAvatar;
    }

    public void setV_uAvatar(Blob v_uAvatar) {
        this.v_uAvatar = v_uAvatar;
    }

    public int getV_id() {
        return v_id;
    }

    public void setV_id(int v_id) {
        this.v_id = v_id;
    }

    public String getV_uName() {
        return v_uName;
    }

    public void setV_uName(String v_uName) {
        this.v_uName = v_uName;
    }

    public String getV_accName() {
        return v_accName;
    }

    public void setV_accName(String v_accName) {
        this.v_accName = v_accName;
    }

    public String getV_title() {
        return v_title;
    }

    public void setV_title(String v_title) {
        this.v_title = v_title;
    }

    public String getV_question() {
        return v_question;
    }

    public void setV_question(String v_question) {
        this.v_question = v_question;
    }

    public List<String> getV_option() {
        return v_option;
    }

    public void setV_option(List<String> v_option) {
        this.v_option = v_option;
    }

    public Timestamp getV_createAt() {
        return this.v_createAt;
    }

    public void setV_createAt(Timestamp v_createAt) {
        this.v_createAt = v_createAt;
    }

    public Timestamp getV_updateAt() {
        return this.v_updateAt;
    }

    public void setV_updateAt(Timestamp v_updateAt) {
        this.v_updateAt = v_updateAt;
    }

    public void info(){
        System.out.println(this.v_uName + " " + this.v_accName + " " + this.v_title + " " + this.v_question + " " + this.v_option + " " + this.v_createAt + " " + this.v_updateAt);
    }
}
