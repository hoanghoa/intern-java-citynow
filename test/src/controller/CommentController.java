package controller;

import models.VoteDAO;
import Object.User;
import Object.Comment;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Timestamp;

@WebServlet(urlPatterns = {"/comment", "/loadMore"})
public class CommentController extends HttpServlet {


    private VoteDAO voteDAO;

    public void init(){
        voteDAO = new VoteDAO("jdbc:mysql://localhost:3306/interndb","root", "2122");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {


        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        HttpSession session = request.getSession();
        int u_id = 0;
        int v_id = (int) session.getAttribute("vote_id");
        String comment_content = request.getParameter("comment_content");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        String usn = (String) session.getAttribute("user_name");
        try {
            u_id = voteDAO.getUserId(usn);
            Comment comment = new Comment();
            comment.setC_uid(u_id);
            comment.setC_vid(v_id);
            comment.setC_content(comment_content);
            comment.setC_createAt(timestamp);

            voteDAO.newComment(comment);

            response.sendRedirect("views/client/home.jsp");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response){
        try{
            String limit = request.getParameter("moreComment");

            int moreLimit = Integer.parseInt(limit);
            int newLimit = 0;

            newLimit = moreLimit + 2;

            System.out.println(newLimit);
            HttpSession session = request.getSession();
            session.setAttribute("limit", newLimit);

            response.sendRedirect("views/client/home.jsp");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
