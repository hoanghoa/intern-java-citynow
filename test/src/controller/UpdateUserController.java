package controller;

import models.UserDAO;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = {"/updateUser"})
public class UpdateUserController extends HttpServlet {
    private UserDAO userDAO;

    public void init(){
        userDAO = new UserDAO("jdbc:mysql://localhost:3306/interndb","root", "2122");
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response){
        String[] listUsn = request.getParameterValues("user_role");

        try{
            userDAO.updateUser(listUsn);
            response.sendRedirect("views/admin/home.jsp");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
