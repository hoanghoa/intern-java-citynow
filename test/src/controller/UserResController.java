package controller;

import models.UserDAO;
import Object.User;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.sql.Blob;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/signUp"})
@MultipartConfig(maxFileSize = 10177215)
public class UserResController extends HttpServlet {
    private UserDAO userResDAO;

    public void init(){
        userResDAO = new UserDAO("jdbc:mysql://localhost:3306/interndb","root", "2122");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String name = request.getParameter("fullName");
        String usn = request.getParameter("accountName");
        String email = request.getParameter("email");
        String resPass = request.getParameter("resPassword");

        InputStream imageFile = null;
        Part avtPart = request.getPart("resAvt");
        InputStream imageContent = avtPart.getInputStream();

        User resUser = new User();
        resUser.setFname(name);
        resUser.setUsn(usn);
        resUser.setEmail(email);
        resUser.setPass(resPass);
        resUser.setROLE(0);

        try{
            if(userResDAO.register(resUser, imageContent)){
                response.sendRedirect("index.jsp");
            }
            else {
                response.sendRedirect("views/client/register.jsp");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
