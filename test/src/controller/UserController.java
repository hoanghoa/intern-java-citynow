package controller;

import models.UserDAO;
import Object.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/login"})
public class UserController extends HttpServlet {
    private static final String login = "/login";
    private static final String register = "/register";
    private UserDAO userDAO;

    public void init(){
        userDAO = new UserDAO("jdbc:mysql://localhost:3306/interndb","root", "2122");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {


        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String usn = request.getParameter("username");
        String pass = request.getParameter("password");
        int checkResult =5;


        User user = new User();
        user.setUsn(usn);
        user.setPass(pass);

        try{
            checkResult = userDAO.validate(user);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(checkResult == -1){
            HttpSession session = request.getSession();
            session.setAttribute("user_name", usn);
            session.setAttribute("user_role", checkResult);
            session.setAttribute("current_page", 1);
            session.setAttribute("limit", 2);
            session.setAttribute("voteLimit", 4);
            try{
                response.sendRedirect("views/admin/home.jsp");
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(checkResult == 0){
            HttpSession session = request.getSession();
            session.setAttribute("user_name", usn);
            session.setAttribute("user_role", checkResult);
            session.setAttribute("current_page", 1);
            session.setAttribute("limit", 2);
            session.setAttribute("voteLimit", 4);
            try{
                response.sendRedirect("views/client/home.jsp");
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(checkResult == 1){
            HttpSession session = request.getSession();
            session.setAttribute("user_name", usn);
            session.setAttribute("user_role", checkResult);
            session.setAttribute("current_page", 1);
            session.setAttribute("limit", 2);
            session.setAttribute("voteLimit", 4);
            try{
                response.sendRedirect("views/client/home.jsp");
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else{
            request.setAttribute("message", "Account is invalid!");
            try{
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }catch (Exception e){
                e.printStackTrace();
            }
//                HttpSession session = request.getSession();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        HttpSession session = request.getSession();
        String usn = (String) session.getAttribute("user_name");
        System.out.println("logout: "+ usn);
        if(usn != null){
            session.removeAttribute(usn);
            response.sendRedirect("index.jsp");
        }
    }
}
