package controller;
import Object.User;
import models.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/createVote"})
public class CreateVoteController extends HttpServlet {

    private UserDAO userVoteCreateDAO;

    public void init(){
        userVoteCreateDAO = new UserDAO("jdbc:mysql://localhost:3306/interndb","root", "2122");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{


        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String usInfo = request.getParameter("createBtn");
        if(!usInfo.substring(0, 1).equals("-")) {
            boolean state = false;
            String usName = usInfo.substring(1, usInfo.length());
            String usRole = usInfo.substring(0, 1);

            User userCreateVote = new User();
            userCreateVote.setUsn(usName);
            userCreateVote.setROLE(Integer.parseInt(usRole));

            try {
                if(userVoteCreateDAO.checkExistUser(userCreateVote)){
                    state = true;
                }
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }

            if(state){
                HttpSession session = request.getSession();
                session.setAttribute("usName", usName);
                session.setAttribute("usRole", Integer.parseInt(usRole));

                response.sendRedirect("views/client/vote.jsp");
            }
            else {
                response.sendRedirect("views/client/notAccess.jsp");
            }
        }
        else{
            String usName = usInfo.substring(2, usInfo.length());
            String usRole = usInfo.substring(0, 2);

            HttpSession session = request.getSession();
            session.setAttribute("usName", usName);
            session.setAttribute("usRole", Integer.parseInt(usRole));

            response.sendRedirect("views/client/vote.jsp");
        }

//        User user = new User();
//        user.setUsn(usn);
    }
}
