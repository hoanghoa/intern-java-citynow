package controller;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = {"/preData", "/nextData"})
public class VoteContentController extends HttpServlet {
    private static final String pre = "/preData";
    private static final String next = "/nextData";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try{

            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");

            String currentPage = request.getParameter("newData");
            int crPage = Integer.parseInt(currentPage);
            int newPage = 0;
            String res = request.getServletPath();

            if(res.equals(pre)){
                newPage = crPage - 1;
            }
            else if(res.equals(next)){
                newPage = crPage + 1;
            }
            System.out.println(newPage);
            HttpSession session = request.getSession();
            session.setAttribute("current_page", newPage);
            session.setAttribute("limit", 2);

            response.sendRedirect("views/client/home.jsp");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
