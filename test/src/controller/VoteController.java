package controller;

import models.UserDAO;
import Object.Vote;
import Object.User;
import Object.VoteContent;
import Object.VoteActivity;
import models.VoteDAO;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;

@WebServlet(urlPatterns = {"/initVote", "/voting", "/moreTopVoted"})
public class VoteController extends HttpServlet {
    private static final String initVote = "/initVote";
    private static final String voting = "/voting";
    private VoteDAO voteDAO;

    public void init(){
        voteDAO = new VoteDAO("jdbc:mysql://localhost:3306/interndb","root", "2122");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response){

        try{
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");

            String action = request.getServletPath();
            if(action.equals(initVote)){
                String title = request.getParameter("vote_title");
                String question = request.getParameter("vote_question");

                int uid = 0;

                HttpSession session = request.getSession();
                String usn = (String) session.getAttribute("user_name");

                try {
                    uid = voteDAO.getUserId(usn);

                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    Vote vote = new Vote();
                    vote.setUid(uid);
                    vote.setTitle(title);
                    vote.setQuestion(question);
                    vote.setCreate_at(timestamp);
                    vote.setUpdate_at(timestamp);

                    int voteId = 0;

                    voteId = voteDAO.newVote(vote);

                    if(voteId != 0){
                        for (int i=1; i<=4; i ++){
                            String vote_op = request.getParameter("vote_op"+i);
                            VoteContent voteContent = new VoteContent();
                            voteContent.setVc_vid(voteId);
                            voteContent.setVc_content(vote_op);

                            voteDAO.newVoteContent(voteContent);
                        }
                    }

                    response.sendRedirect("views/client/home.jsp");

                } catch (SQLException | IOException e) {
                    e.printStackTrace();
                }
            }
            else if(action.equals(voting)){

                request.setCharacterEncoding("UTF-8");
                response.setCharacterEncoding("UTF-8");

                HttpSession session = request.getSession();
                String usn = (String) session.getAttribute("user_name");

                User user = new User();
                user.setUsn(usn);

                int uid = voteDAO.getUserId(usn);
                int vid = (int) session.getAttribute("vote_id");
                String vote_content = request.getParameter("customRadio");
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());

//                System.out.println(uid + " " + vid + " " + vote_content);
                VoteActivity voteActivity = new VoteActivity();
                voteActivity.setVa_uid(uid);
                voteActivity.setVa_vid(vid);
                voteActivity.setVa_content(vote_content);
                voteActivity.setVa_createAt(timestamp);

                int res = voteDAO.addVoteActivity(voteActivity);
                if(res == 1) {
                    response.sendRedirect("views/client/home.jsp");
                }
                else {
                    response.sendRedirect("index.jsp");
                }
            }
            else {
                response.sendRedirect("index.jsp");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response){
        try{
            String voteLimit = request.getParameter("moreComment");

            int moreVotedLimit = Integer.parseInt(voteLimit);

            int newVotedLimit = 0;

            newVotedLimit = moreVotedLimit + 2;

            HttpSession session = request.getSession();
            session.setAttribute("voteLimit", newVotedLimit);

            response.sendRedirect("views/client/home.jsp");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
