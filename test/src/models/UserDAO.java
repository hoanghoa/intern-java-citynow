package models;
import Object.User;

import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    private String url;
    private String db_usn;
    private String db_pass;
    private Connection conn;

    public UserDAO(String url, String db_usn, String db_pass) {
        this.url = url;
        this.db_usn = db_usn;
        this.db_pass = db_pass;
    }

    /*
     * Connect vs disconnect
     */
    public void connect() throws SQLException {
        if(conn == null || conn.isClosed()){
            try{
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            conn = DriverManager.getConnection(url, db_usn, db_pass);
        }
    }

    public void disconnect(){
        try {
            if(conn != null || !conn.isClosed()){
                conn.close();
                conn = null;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    /*end connect region*/

    /*
    * region check login
     */
    public int validate(User user) {
        int user_type = -99;
        try {
            connect();
            PreparedStatement state = conn.prepareStatement("select * from tb_user where u_accountName = ? and u_password = ?");
            state.setString(1, user.getUsn());
            state.setString(2, user.getPass());

            ResultSet result = state.executeQuery();
            if(result.next()){
                user_type = result.getInt(7);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
           disconnect();
        }
     return user_type;
    }
    /*end validate role region*/

    /*
     * region check exist user
     */
    public boolean checkExistUser(User user) throws ClassNotFoundException, SQLException {
        boolean status = false;
        connect();

        PreparedStatement state = conn.prepareStatement("select u_name from tb_user where u_accountName = ? and u_type = ?");
        state.setString(1, user.getUsn());
        state.setInt(2, user.getROLE());

        ResultSet result = state.executeQuery();
        if(result.next()){
            String cName = result.getString(1);
            if(cName != null){
                System.out.println(cName);
                status = true;
            }
            else {
                System.out.println((String) null);
            }
        }

        disconnect();
        return status;
    }
    /*end validate region*/

    /*
    * region register user
     */
    public boolean register(User user, InputStream inputStream) throws SQLException {
        boolean status = false;
        int result = 0;

        connect();
        PreparedStatement state = conn.prepareStatement("INSERT into tb_user (u_id, u_name, u_accountName, u_password, u_avatar, u_email, u_type) values (null,?,?,?, ? ,?,?)");
        state.setString(1, user.getFname());
        state.setString(2, user.getUsn());
        state.setString(3, user.getPass());
        state.setBlob(4, inputStream);
        state.setString(5, user.getEmail());
        state.setInt(6, user.getROLE());


        result = state.executeUpdate();
        System.out.println(result);

        if(result == 1){
            status = true;
        }
        else {
            status = false;
        }

        disconnect();

        return status;
    }
    /*
    * end region register
     */

    /*
    * region list user manager
     */

    public List<User> listUsers() throws SQLException {
        List<User> userList =new ArrayList<>();
        String sqlQuery = "Select u_name, u_accountName, u_avatar, u_email, u_type from tb_user";

        connect();

        Statement state = conn.createStatement();
        ResultSet resultSet = state.executeQuery(sqlQuery);

        while (resultSet.next()){
            String fName     = resultSet.getString(1);
            String uName     = resultSet.getString(2);
            Blob u_avatar  = resultSet.getBlob(3);
            String u_email   = resultSet.getString(4);
            int u_type       = resultSet.getInt(5);

            User user = new User();

            user.setFname(fName);
            user.setUsn(uName);
            user.setAvatar(u_avatar);
            user.setEmail(u_email);
            user.setROLE(u_type);

            userList.add(user);
        }

        disconnect();

        return userList;
    }

    public void updateUser(String[] listUsn) {
        try{
            String query = "update tb_user set u_type = 0 where u_id <> 1";
            String sqlQuery = "update tb_user set u_type = 1 where u_accountName = ?";

            connect();

            PreparedStatement state = conn.prepareStatement(sqlQuery);

            Statement statement = conn.createStatement();
            int result = statement.executeUpdate(query);
            System.out.println(result);
            if(result != 0){
                for(String usn: listUsn){
                    state.setString(1, usn);
                    state.executeUpdate();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            disconnect();
        }
    }
}
