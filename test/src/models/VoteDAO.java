package models;

import Object.VoteActivity;
import Object.Vote;
import Object.VoteContent;
import Object.User;
import Object.Content;
import Object.Comment;
import Object.TopVote;
import Object.TestClass;
import Object.CommentContent;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.plaf.nimbus.State;
import javax.xml.bind.DatatypeConverter;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class VoteDAO {
    private String url;
    private String db_usn;
    private String db_pass;
    private Connection conn;

    public VoteDAO(String url, String db_usn, String db_pass) {
        this.url = url;
        this.db_usn = db_usn;
        this.db_pass = db_pass;
    }

    /*
     * Connect vs disconnect
     */
    public void connect() throws SQLException {
        if(conn == null || conn.isClosed()){
            try{
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            conn = DriverManager.getConnection(url, db_usn, db_pass);
        }
    }

    public void disconnect(){
        try {
            if(conn != null || !conn.isClosed()){
                conn.close();
                conn = null;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    /*end connect region*/

    /*
    * region insert
     */

    public int newVote(Vote vote) throws SQLException {
        int result = 0;
        int vid = 0;
        try{
            connect();
            PreparedStatement state = conn.prepareStatement("INSERT into tb_vote (v_id, v_uid, v_title, v_question, v_createAt, v_updateAt) values (null,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            state.setInt(1, vote.getUid());
            state.setString(2, vote.getTitle());
            state.setString(3, vote.getQuestion());
            state.setTimestamp(4, vote.getCreate_at());
            state.setTimestamp(5, vote.getUpdate_at());


            result = state.executeUpdate();
            if(result == 1){
                ResultSet key = state.getGeneratedKeys();
                while (key.next()){
                    vid = key.getInt(1);
                }
                System.out.println(vid);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            disconnect();
        }

        return vid;
    }


    public void newVoteContent(VoteContent voteContent) {
        int result = 0;
        try{
            connect();
            PreparedStatement state = conn.prepareStatement("INSERT into tb_votecontent (vc_id, vc_vid, vc_content) values (null,?,?)");
            state.setInt(1, voteContent.getVc_vid());
            state.setString(2, voteContent.getVc_content());


            result = state.executeUpdate();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            disconnect();
        }
    }
    /*
     * end region register
     */

    public int getUserId(String usn) {
        int uid = 0;
        try{
            connect();

            PreparedStatement state = conn.prepareStatement("select u_id from tb_user where u_accountName = ?");
            state.setString(1, usn);

            ResultSet result = state.executeQuery();
            if(result.next()){
                uid = result.getInt(1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            disconnect();
        }

        return uid;
    }

    public String getUserAvatar(int uid){
        String userAvtString = "data:image/png;base64,";
        try{
            connect();
            Blob user_avt = null;
            PreparedStatement statement = conn.prepareStatement("select u_avatar from tb_user where u_id = ?");
            statement.setInt(1, uid);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                user_avt = result.getBlob(1);
            }
            byte[] imageBytes = user_avt.getBytes(1, (int) user_avt.length());
            String encodeImage = DatatypeConverter.printBase64Binary(imageBytes);

            userAvtString = userAvtString + encodeImage;

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            disconnect();
        }
        return userAvtString;
    }

    /*
    * get list vote content
     */
    public List<Content> getListVoteData(int offset){
        List<Content> contentListDatas = new ArrayList<Content>();
        if(offset >= 1) {
            offset--;
            try {
                connect();

                PreparedStatement voteState = conn.prepareStatement("select * from tb_vote order by v_id desc limit 1 offset ?");
                PreparedStatement userState = conn.prepareStatement("select u_name, u_accountName, u_avatar from tb_user where u_id = ?");
                PreparedStatement voteContentState = conn.prepareStatement("select vc_content from tb_votecontent where vc_vid = ?");


                voteState.setInt(1, offset);
                ResultSet voteResult = voteState.executeQuery();
                while (voteResult.next()) {
                    int v_id = 0;
                    int v_uid = 0;
                    String v_title = null;
                    String v_question = null;
                    String u_name = null;
                    String u_accountName = null;
                    Timestamp v_createAt = null;
                    Timestamp v_updateAt = null;
                    Blob u_avatar = null;

                    List<String> vc_option = new ArrayList<String>();

                    v_id = voteResult.getInt(1);
                    v_uid = voteResult.getInt(2);
                    v_title = voteResult.getString(3);
                    v_question = voteResult.getString(4);
                    v_createAt = voteResult.getTimestamp(5);
                    v_updateAt = voteResult.getTimestamp(6);

                    userState.setInt(1, v_uid);
                    ResultSet userResult = userState.executeQuery();
                    while (userResult.next()) {
                        u_name = userResult.getString(1);
                        u_accountName = userResult.getString(2);
                        u_avatar = userResult.getBlob(3);
                    }

                    voteContentState.setInt(1, v_id);
                    ResultSet voteContentResult = voteContentState.executeQuery();
                    while (voteContentResult.next()) {
                        String content = voteContentResult.getString(1);
                        vc_option.add(content);
                    }

                    Content content = new Content();

                    content.setV_id(v_id);
                    content.setV_uName(u_name);
                    content.setV_accName(u_accountName);
                    content.setV_uAvatar(u_avatar);
                    content.setV_title(v_title);
                    content.setV_question(v_question);
                    content.setV_option(vc_option);
                    content.setV_createAt(v_createAt);
                    content.setV_updateAt(v_updateAt);

                    contentListDatas.add(content);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                disconnect();
            }
        }
        return contentListDatas;
    }

    /*
    * comment region
     */
    public void newComment(Comment comment){
        int result = 0;
        try{
            connect();
            PreparedStatement state = conn.prepareStatement("INSERT into tb_comment (c_id, c_uid, c_vid, c_content, c_createAt) values (null,?,?,?,?)");
            state.setInt(1, comment.getC_uid());
            state.setInt(2, comment.getC_vid());
            state.setString(3, comment.getC_content());
            state.setTimestamp(4, comment.getC_createAt());


            result = state.executeUpdate();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            disconnect();
        }
    }


    public List<CommentContent> getListCommentData(int vote_id, int limit){
        List<CommentContent> commentContentList = new ArrayList<CommentContent>();

        try {
            connect();

            PreparedStatement commentState = conn.prepareStatement("select * from tb_comment where c_vid = ? order by c_createAt desc limit ?");
            commentState.setInt(1, vote_id);
            commentState.setInt(2, limit);
            ResultSet result = commentState.executeQuery();

            PreparedStatement userState = conn.prepareStatement("select u_name, u_avatar from tb_user where u_id = ?");

            while (result.next()) {
                int c_id = 0;
                int c_uid = 0;
                String c_content = null;
                Timestamp c_createAt = null;
                Blob u_avatar = null;

                String u_name = null;


                c_id = result.getInt(1);
                c_uid = result.getInt(2);
                c_content = result.getString(4);
                c_createAt = result.getTimestamp(5);

                userState.setInt(1, c_uid);
                ResultSet userResult = userState.executeQuery();
                while (userResult.next()) {
                    u_name = userResult.getString(1);
                    u_avatar = userResult.getBlob(2);
                }

                CommentContent commentContent = new CommentContent();

                commentContent.setC_id(c_id);
                commentContent.setC_uAvatar(u_avatar);
                commentContent.setC_fullName(u_name);
                commentContent.setC_time(c_createAt);
                commentContent.setC_content(c_content);

                commentContentList.add(commentContent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            disconnect();
        }
        return commentContentList;
    }

    /*
    * end comment region
     */

    /*
    * region activity vote
     */
    public int addVoteActivity(VoteActivity voteActivity){
        int result = 0;
        try{
            connect();

            PreparedStatement state = conn.prepareStatement("insert into tb_voteactivity(va_id, va_uid, va_vid, va_content, va_createAt) values (null, ?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            state.setInt(1, voteActivity.getVa_uid());
            state.setInt(2, voteActivity.getVa_vid());
            state.setString(3, voteActivity.getVa_content());
            state.setTimestamp(4, voteActivity.getVa_createAt());

            result = state.executeUpdate();

//            if(result == 1){
//                ResultSet key = state.getGeneratedKeys();
//                while (key.next()){
//                    va_id = key.getInt(1);
//                }
//            }

        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            disconnect();
        }
        return result;
    }

    public Timestamp getLastVotedTime(int vote_id){
        Timestamp lastTime = null;
        try{
            connect();

            PreparedStatement statement = conn.prepareStatement("select va_createAt from tb_voteactivity where va_vid = ?");
            statement.setInt(1, vote_id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                lastTime = resultSet.getTimestamp(1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            disconnect();
        }
        return lastTime;
    }

    public String getVotedContentByUser(int uid, int vid){
        String content = null;
        try{
            connect();

            PreparedStatement statement = conn.prepareStatement("select va_content from tb_voteactivity where va_uid = ? and va_vid = ?");
            statement.setInt(1, uid);
            statement.setInt(2, vid);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                content = resultSet.getString(1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            disconnect();
        }
        return content;
    }

    public int getVotedNum(int vid){
        int votedNum = 0;
        try{
            connect();

            PreparedStatement statement = conn.prepareStatement("select count(*) from tb_voteactivity where va_vid = ?");
            statement.setInt(1, vid);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()){
                votedNum = resultSet.getInt(1);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            disconnect();
        }
        return votedNum;
    }

    public List<TopVote> getTopVotedList(int voteLimit){
        List<TopVote> topVoteArrayList = new ArrayList<TopVote>();
        try{
            connect();

            PreparedStatement voteNumState = conn.prepareStatement("select count(*) as votedNum, va_vid from tb_voteactivity group by va_vid order by votedNum desc limit ?");
            PreparedStatement commentNumState = conn.prepareStatement("select count(*) as commentedNum from tb_comment where c_vid = ?");
            PreparedStatement voteState = conn.prepareStatement("select v_uid from tb_vote where v_id = ?");
            PreparedStatement userState = conn.prepareStatement("select u_name, u_avatar from tb_user where u_id = ?");

            voteNumState.setInt(1, voteLimit);
            ResultSet voteNumRes = voteNumState.executeQuery();

            while (voteNumRes.next()){
                ResultSet commentNumRes = null;
                ResultSet voteRes = null;
                ResultSet userRes = null;
                int uid = 0;
                int votedNum = 0;
                int commentedNum = 0;
                String uName = null;
                Blob uAvatar = null;

                votedNum = voteNumRes.getInt(1);

                int vid = voteNumRes.getInt(2);

                commentNumState.setInt(1, vid);
                commentNumRes = commentNumState.executeQuery();
                while (commentNumRes.next()){
                    commentedNum = commentNumRes.getInt(1);
                }

                voteState.setInt(1, vid);
                voteRes = voteState.executeQuery();
                while (voteRes.next()){
                    uid = voteRes.getInt(1);
                }

                userState.setInt(1, uid);
                userRes = userState.executeQuery();
                while (userRes.next()){
                    uName = userRes.getString(1);
                    uAvatar = userRes.getBlob(2);
                }

                TopVote topVote = new TopVote();
                topVote.setFullName(uName);
                topVote.setAvatar(uAvatar);
                topVote.setVotedNum(votedNum);
                topVote.setCommentedNum(commentedNum);

                topVoteArrayList.add(topVote);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            disconnect();
        }
        return topVoteArrayList;
    }


    /*
    * end activity
     */
}
