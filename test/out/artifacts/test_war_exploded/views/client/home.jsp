<%@ page import="models.VoteDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="Object.Content" %>
<%@ page import="java.util.Date" %>
<%@ page import="Object.CommentContent" %>
<%@ page import="Object.TopVote" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.sql.Timestamp" %>
<%@ page import="java.sql.Blob" %>
<%@ page import="javax.xml.bind.DatatypeConverter" %>
<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: hoang
  Date: 7/22/2020
  Time: 2:56 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String usn = (String) session.getAttribute("user_name");
    int user_role = 10;
    user_role = (int) session.getAttribute("user_role");
    if(usn != null && user_role != 10){
    System.out.println("login_user: "+ usn + " " + user_role);

    int currentPage = (int) session.getAttribute("current_page");
    int limit = (int) session.getAttribute("limit");
    int voteLimit = (int) session.getAttribute("voteLimit");

    VoteDAO voteDAO = new VoteDAO("jdbc:mysql://localhost:3306/interndb", "root", "2122");

    int uid = voteDAO.getUserId(usn);

    String userAvt = voteDAO.getUserAvatar(uid);

    List<Content> listVoteData = null;
    List<TopVote> topVoteList = null;
    listVoteData = voteDAO.getListVoteData(currentPage);
    if(listVoteData.isEmpty()){
        currentPage = 1;
        listVoteData = voteDAO.getListVoteData(currentPage);
    }

    List<String> listVoteOption = new ArrayList<String>();

    for(Content content: listVoteData){
    listVoteOption = content.getV_option();
    int vid = content.getV_id();
    session.setAttribute("vote_id", vid);

    topVoteList = voteDAO.getTopVotedList(voteLimit);

    String option = voteDAO.getVotedContentByUser(uid, vid);

    int votedNum = voteDAO.getVotedNum(vid);

    Date date_update = null;
    if(voteDAO.getLastVotedTime(vid) != null) {
        date_update = new Date((long) voteDAO.getLastVotedTime(vid).getTime());
    }
    else {
        date_update = new Date((long)content.getV_updateAt().getTime());
    }

    List<CommentContent> commentContentList = null;
    commentContentList = voteDAO.getListCommentData(vid, limit);

    Date date_create = new Date((long)content.getV_createAt().getTime());
    DateFormat d = new SimpleDateFormat("dd-MM-yyyy");
    DateFormat t = new SimpleDateFormat("HH:mm");
    DateFormat lastTime = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    Timestamp now = new Timestamp(System.currentTimeMillis());
    Date today = new Date(now.getTime());

    double dateDifferenceTime = today.getTime() - date_update.getTime();
    double dateDiff = (dateDifferenceTime/(1000*60*60*24));
    float dateDiffRes = (float) Math.round(dateDiff*10)/10;
    String dateDiffString = null;
    if(dateDiffRes >= 1 && dateDiffRes <= 1.5){
        dateDiffString = (int) dateDiffRes + " day left";
    }
    else if(dateDiffRes > 1.5){
        dateDiffString = (int) dateDiffRes + 1 + " days left";
    } else if(dateDiffRes >= 0.5 && dateDiffRes < 1){
        dateDiffString = Math.round((dateDiffRes*24)*10)/10 + " hours left";
    }
    else {
        dateDiffString = "Recently";
    }

%>


<!DOCTYPE html>
<html lang="vn">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <title>Home page</title>
</head>
<script type="text/javascript">
    window.addEventListener("keydown", function (e) {
        if(e.keyCode === 13){
            var submit = document.getElementById("comment_form");
            submit.submit();
        }
    }, false);
</script>
<body>
<!-- Navbar -->
<nav class="container navbar navbar-default navbar-fixed-top navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Welcome <b style="font-size: 25px;"><%= usn %></b></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav mr-auto">
    </ul>
    <div class="form-inline my-2 my-lg-0">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <form action="<%=request.getContextPath()%>/createVote" method="get">
                    <%
                        if(user_role == -1|| user_role == 1){
                    %>
                    <button class="btn btn-outline-primary my-2 my-sm-0" name="createBtn" value="<%= user_role+usn%>" type="submit">Create</button>
                    <%
                        }else{
                    %>
                    <%
                        }
                    %>
                </form>
            </li>
            <li class="nav-item active" style="margin-left: 5px;">
                <form action="<%=request.getContextPath()%>/login" method="get">
                    <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Logout</button>
                </form>
            </li>
        </ul>
    </div>
</nav>
<!-- Vote region -->
<div class="container border border-dark">
    <div class="row container">
        <!-- Option, comment region -->
        <div class="column col-sm-9" style="margin: 10px;margin-top: 30px; margin-right: 0; padding-right: 5px;">
            <!-- option form -->
            <div class="col-sm-12 m-0 p-0.5 border border-dark">
                <div style="margin-top: 5px;" class="row d-flex justify-content-end">
                    <form action="<%=request.getContextPath()%>/preData" method="get">
                        <button class="btn btn-outline-primary my-2 my-sm-0" value="<%= currentPage%>" name="newData" type="submit">
                            Previous
                        </button>
                    </form>
                    <form action="<%=request.getContextPath()%>/nextData" method="get">
                        <button class="btn btn-outline-primary my-2 my-sm-0" style="margin-right: 5px; margin-left: 30px;" value="<%= currentPage%>" name="newData" type="submit">
                            Next
                        </button>
                    </form>
                </div>
                <div class="col-sm-12 m-0 row" style="padding-top: 10px; padding-left: 0px;">
                    <div class="p-0.5">
                        <div class="m-0 p-0.5 row">
                            <%
                                Blob voteImage = content.getV_uAvatar();
                                String voteImageString = null;
                                try {
                                    byte[] imageBytes = voteImage.getBytes(1, (int) voteImage.length());
                                    String encodeImage = DatatypeConverter.printBase64Binary(imageBytes);
                                    voteImageString = "data:image/png;base64,"+encodeImage;
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                            %>
                            <img class="rounded-circle" style="height: 100px; width: 100px;" src="<%= voteImageString%>">
                            <div class="column" style="padding-left: 15px;">
                                <div class="row m-0">
                                    <p style="font-size: 30px;"><b><%= content.getV_uName()%></b></p>
                                    <p style="margin-top: 20px; margin-left: 5px; font-size: 11px;">@<%= content.getV_accName()%></p>
                                </div>
                                <p style="font-size: 13px; margin-top: -15px;">
                                   <%= d.format(date_create)%>  - <%= t.format(date_create)%>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- region vote content -->
                <form action="<%= request.getContextPath()%>/voting" method="post" accept-charset="utf-8">
                    <div class="column m-0 p-0 mb-2">
                        <p class="row m-0 p-0 mb-2" style="font-size: 30px;"><b><%= content.getV_title()%></b></p>
                        <p class="row m-0 p-0" style="font-size: 15px;"><%= content.getV_question()%></p>
                    </div>
                    <!-- region vote content -->

                    <%
                        for(int i = 0; i<listVoteOption.size(); i++){
                    %>
                    <div class="custom-control custom-radio">
                        <%
                            if(option != null){
                                if(option.equals(listVoteOption.get(i))){
                        %>
                        <input checked="checked" type="radio" id="customRadio<%= i%>" name="customRadio" class="custom-control-input" value="<%= listVoteOption.get(i)%>">
                        <label class="custom-control-label" for="customRadio<%= i%>"><%= listVoteOption.get(i)%></label>
                        <%
                            }
                            else {
                        %>
                        <input type="radio" id="customRadio<%= i%>" name="customRadio" class="custom-control-input" value="<%= listVoteOption.get(i)%>">
                        <label class="custom-control-label" for="customRadio<%= i%>"><%= listVoteOption.get(i)%></label>
                        <%
                                }
                            }
                            else {
                        %>
                        <input type="radio" id="customRadio<%= i%>" name="customRadio" class="custom-control-input" value="<%= listVoteOption.get(i)%>">
                        <label class="custom-control-label" for="customRadio<%= i%>"><%= listVoteOption.get(i)%></label>
                        <%
                            }
                        %>
                    </div>
                    <%
                        }
                        if(option!=null){
                    %>
                    <div class="row m-0 p-0.5">
                        <button type="submit" disabled="disabled" class="btn btn-outline-success" style="height: 40px; width: 80px;margin-top: 10px; margin-bottom: 8px;">
                            Vote
                        </button>
                        <div class=" row" style="padding: 15px; margin-left: 30px;">
                            <p class="mb-0"><%= votedNum %> votes</p>
                            <i style="width: 2px; height: 2px; margin-left: 20px; margin-top: 5px;" class="fa fa-circle"></i>
                            <p style="margin-left: 30px;"><%= dateDiffString%></p>
                        </div>
                    </div>
                    <%
                        }
                        else {
                    %>
                    <div class="row m-0 p-0.5">
                        <button type="submit" class="btn btn-outline-success" style="height: 40px; width: 80px;margin-top: 10px; margin-bottom: 8px;">
                            Vote
                        </button>
                        <div class=" row" style="padding: 15px; margin-left: 30px;">
                            <p class="mb-0"><%= votedNum %> votes</p>
                            <i style="width: 2px; height: 2px; margin-left: 20px; margin-top: 5px;" class="fa fa-circle"></i>
                            <p style="margin-left: 30px;"><%= dateDiffString%></p>
                        </div>
                    </div>
                    <%
                        }
                    %>
                </form>
            </div>
            <!-- end option region -->
            <!-- region comment vs write comment -->
            <%
                for(CommentContent commentContent: commentContentList){
            %>
            <div class="col-sm-12 border border-dark" style="padding: 8px;">
                <div class="m-0 p-0.5 row">
                    <%
                        Blob commentImage = commentContent.getC_uAvatar();
                        String commentImageString = null;
                        try {
                            byte[] imageBytes = commentImage.getBytes(1, (int) commentImage.length());
                            String encodeImage = DatatypeConverter.printBase64Binary(imageBytes);
                            commentImageString = "data:image/png;base64,"+encodeImage;
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    %>
                    <img class="rounded-circle" style="height: 40px; width: 40px;" src="<%= commentImageString%>">
                    <div class="column m-0 p-0.5" style="padding-left: 10px;">
                        <p class="m-0 p-0.5" style="font-size: 13px;">
                            <%= commentContent.getC_fullName()%>
                        </p>
                        <p class="m-0 p-0.5" style="font-size: 10px;">
                            <%= d.format(commentContent.getC_time())%> - <%= t.format(commentContent.getC_time())%>
                        </p>
                    </div>
                </div>
                <div>
                    <div class="m-0 p-0.5">
                        <p class="m-0 p-0.5" style="font-size: 15px; padding-top: 5px;">
                            <%= commentContent.getC_content()%>
                        </p>
                    </div>
                </div>
            </div>
            <%
                    }
            %>
            <div class="input-group mb-3 col-sm-12 border border-dark" style="padding: 0; margin: 0;">
                <form action="<%=request.getContextPath()%>/loadMore" method="get">
                    <button class="btn" style="background-color:transparent;"  value="<%= limit%>" name="moreComment" type="submit">
                        load more...
                    </button>
                </form>
            </div>
            <div class="input-group mb-3 col-sm-12 border border-dark" style="padding: 8px; margin-bottom: 0px;">
                <img class="rounded-circle" style="height: 40px; width: 40px; margin-right: 10px;" src="<%= userAvt%>" alt="avatar">
                <form id="comment_form" action="<%= request.getContextPath()%>/comment" method="post">
                    <input type="text" id="comment_content" name="comment_content" class="form-control" style="width: 700px;" placeholder="Comment..." aria-label="Comment" aria-describedby="basic-addon2">
                </form>
            </div>
        </div>
        <!-- end comment region -->
        <!-- Top vote region -->
        <div class="col-sm-2" style="margin: 10px; margin-top: 30px; margin-left: 0;">
            <div style="padding: 0px; width: 250px;">
                <div class="col-sm-12 border border-dark mp-0.5">
                    <p class="d-flex justify-content-center align-item-center my-1">Top votes</p>
                </div>
                <%
                    for(TopVote topVote: topVoteList){
                %>
                <div class="col-sm-12 border-right border-left border-bottom border-dark" style="padding: 8px;">
                    <div class="p-0.5">
                        <div class="m-0 p-0.5 row">
                            <%
                                Blob topVoteImage = topVote.getAvatar();
                                String topVoteImageString = null;
                                try {
                                    byte[] imageBytes = topVoteImage.getBytes(1, (int) topVoteImage.length());
                                    String encodeImage = DatatypeConverter.printBase64Binary(imageBytes);
                                    topVoteImageString = "data:image/png;base64,"+encodeImage;
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                            %>
                            <img class="rounded-circle" style="height: 50px; width: 50px;" src="<%= topVoteImageString%>">
                            <div class="column m-0 p-0.5" style="padding-left: 15px;">
                                <p class="m-0 p-0.5">
                                    <%= topVote.getFullName()%>
                                </p>
                                <p class="row m-0 p-0.5" style="font-size: 10px;">
                                    <%= topVote.getVotedNum() %> VOTES - <%= topVote.getCommentedNum() %> COMMENTS
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <%
                        }
                %>
                <div class="border border-dark">
                    <form action="<%=request.getContextPath()%>/moreTopVoted" method="get">
                        <button class="btn" style="background-color:transparent;"  value="<%= voteLimit%>" name="moreComment" type="submit">
                            load more...
                        </button>
                    </form>
                </div>
                <%
                    }
                %>
            </div>
        </div>
        <!-- end top vote region -->
    </div>
</div>
</body>

</html>
<%

}else {
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<p>
    OPP! Something went wrong
</p>
<a href="../../index.jsp">Go Back</a>
</body>
</html>
<%
    }
%>
