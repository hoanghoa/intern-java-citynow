<%--
  Created by IntelliJ IDEA.
  User: hoang
  Date: 7/22/2020
  Time: 2:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import = "java.sql.SQLException" %>
<%@page import = "java.util.ArrayList" %>
<%@page import = "Object.User" %>
<%@page import = "models.UserDAO" %>
<%@ page import="java.sql.Blob" %>
<%@ page import="javax.xml.bind.DatatypeConverter" %>

<%
    String usn = (String) session.getAttribute("user_name");
    int user_role = 10;
    user_role = (int) session.getAttribute("user_role");

    if(usn != null && user_role != 10){
    System.out.println("login_admin: "+ usn + " " + user_role);
    ArrayList<User> arrayList = new ArrayList<>();
    UserDAO userDAO = new UserDAO("jdbc:mysql://localhost:3306/interndb", "root", "2122");
    try {
        arrayList = (ArrayList<User>) userDAO.listUsers();
    } catch (SQLException e) {
        e.printStackTrace();
    }
//    System.out.println(arrayList.get(0).getFname() + " "+ arrayList.get(0).getROLE());

    if(usn.equals(arrayList.get(0).getUsn()) && user_role == arrayList.get(0).getROLE()){
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>User Manager</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="stylesheet" href="../../css/usermanager.css">
</head>
<body>
<div class="container-xl">
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>User <b>Management</b></h2>
                    </div>
                    <h2 style="padding-left: 260px;">
                        <form action="<%=request.getContextPath()%>/adminPage" method="post">
                            <button style="" type="submit" name="adUsn" value="<%= arrayList.get(0).getUsn()%>">
                                    Home
                            </button>
                        </form>
                    </h2>
                </div>
            </div>
            <form action="<%= request.getContextPath()%>/updateUser" method="post">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Avatar</th>
                        <th>Full name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>
                            <input type="checkbox" id="checkAll" onclick="check()">
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        for (int counter = 1; counter < arrayList.size(); counter++) {
                    %>
                    <tr>
                        <td>
                            <%=counter%>
                        </td>
                        <td>
                            <%
                                Blob image = arrayList.get(counter).getAvatar();
                                String imageString = null;
                                try {
                                    byte[] imageBytes = image.getBytes(1, (int) image.length());
                                    String encodeImage = DatatypeConverter.printBase64Binary(imageBytes);
                                    imageString = "data:image/png;base64,"+encodeImage;
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                            %>
                            <img style="height: 50px; width: 50px;" src="<%= imageString%>" alt="">
                        </td>
                        <td>
                            <%= arrayList.get(counter).getFname() %>
                        </td>
                        <td>
                            <%= arrayList.get(counter).getUsn() %>
                        </td>
                        <td>
                            <%= arrayList.get(counter).getEmail() %>
                        </td>
                        <td>
                            <%
                                if(arrayList.get(counter).getROLE() == 1){
                            %>
                            <input id="user_role" name="user_role" type="checkbox" checked="checked" value="<%= arrayList.get(counter).getUsn()%>">
                            <%
                            }
                            else if(arrayList.get(counter).getROLE() == 0){
                            %>
                            <input id="user_role" name="user_role" type="checkbox" value="<%= arrayList.get(counter).getUsn()%>">
                            <%
                                }
                            %>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                    </tbody>
                </table>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-outline-success">
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">

    function check() {
        var checkboxes = document.getElementsByTagName('input');
        if(document.getElementById("checkAll").checked){
            // checkAll();
            for(var i = 0; i < checkboxes.length; i++){
                if(checkboxes[i].type === "checkbox"){
                    checkboxes[i].checked = true;
                }
            }
        }
        else if(!document.getElementById("checkAll").checked){
            // uncheckAll();
            for(var j = 0; j < checkboxes.length; j++){
                if(checkboxes[j].type === "checkbox"){
                    checkboxes[j].checked = false;
                }
            }
        }
    }

</script>
</body>
</html>
<%
    System.out.println(usn + " " + user_role);
    }
    else{
%>
<!DOCTYPE html>
<html lang="id" dir="ltr">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- Title -->
    <title>Sorry, This Page Can&#39;t Be Accessed</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous" />
</head>

<body class="bg-dark text-white py-5">
<div class="container py-5">
    <div class="row">
        <div class="col-md-2 text-center">
            <p><i class="fa fa-exclamation-triangle fa-5x"></i><br/>Status Code: 403</p>
        </div>
        <div class="col-md-10">
            <h3>OPPSSS!!!! Sorry...</h3>
            <p>Sorry, your access is refused due to security reasons of our server and also our sensitive data.<br/>Please go back to the previous page to continue browsing.</p>
            <a class="btn btn-danger" href="javascript:history.back()">Go Back</a>
        </div>
    </div>
</div>

</body>

</html>
<%
        System.out.println(usn + " " + user_role);
    }
    }else {
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<p>
    OPP! Something went wrong
</p>
<a href="../../index.jsp">Go Back</a>
</body>
</html>
<%
    }
%>