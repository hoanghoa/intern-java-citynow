<%
%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <script src="https://kit.fontawesome.com/8b1db1262c.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/loginstyle.css">
    <title>Login page</title>
</head>
<script type="text/javascript">
    // function noBack()
    // {
    //     window.history.forward()
    // }
    // noBack();
    // window.onload = noBack;
    // window.onpageshow = function(evt) { if (evt.persisted) noBack() }
    // window.onunload = function() { void (0) }
</script>
<body>
<div class="container">
    <h1 style="color:#000">LOG IN</h1>
    <form action="<%=request.getContextPath()%>/login" method="post">

        <div class="tbox">
            <input type="text" placeholder="username" name="username" required>
        </div>

        <div class="tbox">
            <input type="password" placeholder="password" name="password" required>
        </div>

        <input class="btn" type="submit" name="" value="Login">
    </form>
    <div class="register">
        <a href="views/client/register.jsp">
            <h2>Register</h2>
        </a>
    </div>

<%--    <a class="b1" href="#">FORGOT PASSWORD ? </a>--%>
<%--    <a class="b2" href="#">CREATE AN ACCOUNT </a>--%>

</div>
</body>
</html>
