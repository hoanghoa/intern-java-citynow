package com.example.diparttern;

public class ClientAImp implements Client{
    private Service service;

    public ClientAImp (Service s){
        this.service = s;
    }

    @Override
    public void doSomething() {
        String info = service.getResult();
        System.out.println(info);
    }
}
