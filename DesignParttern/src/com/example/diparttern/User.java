package com.example.diparttern;

public class User {
    public static void main(String[] args) {
        Service service = new ServiceImp();
        Client client = new ClientAImp(service);
        client.doSomething();

        Service serviceb = new ServiceBImp();
        Client clientB = new ClientAImp(serviceb);
        clientB.doSomething();

    }
}
