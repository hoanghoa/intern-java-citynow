package com.example.builder;


public interface StudentBuilder {

    StudentBuilder setId(int id);

    StudentBuilder setName(String name);

    StudentBuilder setMajor(String major);

    StudentBuilder setAge(int age);

    StudentBuilder setPhone(String phone);

    StudentBuilder setAddress(String address);

    StudentBuilder setFinish(boolean finish);

    Student build();
}
