package com.example.builder;

public class Student {
    private int id;
    private String name;
    private String major;
    private int age;
    private String phone;
    private String address;
    private boolean finish;

    public Student(int id, String name, String major, int age, String phone, String address, boolean finish){
        this.id = id;
        this.name = name;
        this.major = major;
        this.age = age;
        this.phone = phone;
        this.address = address;
        this.finish = finish;
    }

    public String info(){
        return "id: "+id+"\n"
                +"name: "+name+"\n"
                +"major: "+major+"\n"
                +"age: "+age+"\n"
                +"phone: "+phone+"\n"
                +"address: "+address+"\n"
                +"finish: "+finish+"\n";
    }

//    public StudentBuilder age(int age){
//        this.age = age;
//        return this;
//    }
//
//    public StudentBuilder major(String major){
//        this.major = major;
//        return this;
//    }
//
//    public StudentBuilder phone(String phone){
//        this.phone = phone;
//        return this;
//    }
//
//    public StudentBuilder address(String address){
//        this.address = address;
//        return this;
//    }
//
//    public Student build(){
//        return new Student(this);
//    }
}
