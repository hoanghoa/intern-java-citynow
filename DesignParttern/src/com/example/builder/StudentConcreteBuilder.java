package com.example.builder;

public class StudentConcreteBuilder implements StudentBuilder {

    private int id;
    private String name;
    private String major;
    private int age;
    private String phone;
    private String address;
    private boolean finish;

    @Override
    public StudentBuilder setId(int id) {
        this.id = id;
        return this;
    }

    @Override
    public StudentBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public StudentBuilder setMajor(String major) {
        this.major = major;
        return this;
    }

    @Override
    public StudentBuilder setAge(int age) {
        this.age = age;
        return this;
    }

    @Override
    public StudentBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    @Override
    public StudentBuilder setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public StudentBuilder setFinish(boolean finish) {
        this.finish = finish;
        return this;
    }

    @Override
    public Student build(){
        return new Student(id, name, major, age, phone, address, finish);
    }
}
