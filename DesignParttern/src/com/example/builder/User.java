package com.example.builder;

public class User {
    public static void main(String[] args) {

        Student studentBuilder1 = new StudentConcreteBuilder()
                .setId(1)
                .setName("Hoang Hoa")
                .setFinish(false)
                .setMajor("CNTT")
                .build();
        System.out.println(studentBuilder1.info());

        Student studentBuilder2 = new StudentConcreteBuilder()
                .setId(2)
                .setName("Hieu")
                .setFinish(true)
                .setMajor("An ninh")
                .setAge(23)
                .build();
        System.out.println(studentBuilder2.info());
    }
}
