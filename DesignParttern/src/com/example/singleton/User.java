package com.example.singleton;

public class User {
    public static void main(String[] args) {
        String demoSingleton = DemoSingleton.getInstance().getMessInstance();
        System.out.println(demoSingleton);
    }
}
