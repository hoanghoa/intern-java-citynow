package com.example.singleton;

import java.util.Random;

public class DemoSingleton {
    private int index;
    private static DemoSingleton instance;

    private DemoSingleton(int index){
        this.index = index;
    }

    public static DemoSingleton getInstance(){
        if(instance == null){
            Random ran = new Random(10000);
            instance = new DemoSingleton(ran.nextInt());
        }
        return instance;
    }

    public String getMessInstance(){
        return "Singleton: " + index;
    }
}
