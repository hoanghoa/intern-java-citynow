package com.example.interfacedemo;

public class InterfaceConcrete implements DemoInterface{
    @Override
    public void show() {
        System.out.println("Demo Interface");
    }
}
