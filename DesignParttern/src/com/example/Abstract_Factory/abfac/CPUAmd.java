package com.example.Abstract_Factory.abfac;

public class CPUAmd implements CPU {
    @Override
    public void create() {
        System.out.println("AMD threadripper 3990x ");
    }
}
