package com.example.Abstract_Factory.abfac;

public class FactoryAMD extends AbstractFactory {
    @Override
    public CPU createCPU() {
        return new CPUAmd();
    }

    @Override
    public CARD createCARD() {
        return new CARDAmd();
    }
}
