package com.example.Abstract_Factory.abfac;

public interface CPU {
    public void create();
}
