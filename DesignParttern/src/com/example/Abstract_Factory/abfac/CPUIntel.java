package com.example.Abstract_Factory.abfac;

public class CPUIntel implements CPU {
    @Override
    public void create() {
        System.out.println("I9-9900k");
    }
}
