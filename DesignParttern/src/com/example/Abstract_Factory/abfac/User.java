package com.example.Abstract_Factory.abfac;

public class User {
    public static void main(String[] args) {
        AbstractFactory abstractFactory = AbstractFactory.getFactory(FactoryName.AMD);
        if (abstractFactory != null) {
            CPU cpu = abstractFactory.createCPU();
            CARD card = abstractFactory.createCARD();
            card.create();
            cpu.create();
        }
    }
}
