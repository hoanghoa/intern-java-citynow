package com.example.Abstract_Factory.abfac;

public abstract class AbstractFactory {
    private static final FactoryAMD AMD = new FactoryAMD();
    private static final FactoryINTEL INTEL = new FactoryINTEL();

    static AbstractFactory getFactory(FactoryName name){
        AbstractFactory factory = null;
        switch (name){
            case AMD:
                factory = AMD;
                break;
            case INTEL:
                factory = INTEL;
                break;
        }
        return factory;
    }

    public abstract CPU createCPU();
    public abstract CARD createCARD();

}
