package com.example.Abstract_Factory.abfac;

public class FactoryINTEL extends AbstractFactory {
    @Override
    public CPU createCPU() {
        return new CPUIntel();
    }

    @Override
    public CARD createCARD() {
        return new CARDIntel();
    }
}
