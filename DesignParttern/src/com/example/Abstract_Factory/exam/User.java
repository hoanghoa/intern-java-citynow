package com.example.Abstract_Factory.exam;

public class User {
    public static void main(String[] args) {
        FactoryShape factoryShape = new FactoryShape();
        Shape shape = factoryShape.getShape("SQUARE");
        shape.draw();
    }
}
