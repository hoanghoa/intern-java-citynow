package com.example.Abstract_Factory.exam;

public class ShapeCircle implements Shape {
    @Override
    public void draw() {
        System.out.println("Drawing Circle");
    }
}
