package com.example.Abstract_Factory.exam;

public class FactoryShape {

    public Shape getShape(String shapeType){
        switch (shapeType){
            case "RECTANGLE":
                return new ShapeRectangle();
            case "SQUARE":
                return new ShapeSquare();
            case "CIRCLE":
                return new ShapeCircle();
            default:
                return null;
        }
    }
}
