package com.example.Abstract_Factory.exam;

public class ShapeRectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Drawing Rectangle");
    }
}
