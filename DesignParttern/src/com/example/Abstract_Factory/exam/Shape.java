package com.example.Abstract_Factory.exam;

public interface Shape {
    public void draw();
}
