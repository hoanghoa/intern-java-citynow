package com.example.Abstract_Factory.exam;

public class ShapeSquare implements Shape {
    @Override
    public void draw() {
        System.out.println("Drawing Square");
    }
}
